package com.tinkerpop.pipes.filter;

import static org.junit.Assert.assertEquals;

import com.tinkerpop.pipes.AbstractPipe;
import com.tinkerpop.pipes.Pipe;
import com.tinkerpop.pipes.impl.filter.CyclicPathFilterPipe;
import com.tinkerpop.pipes.impl.util.Pipeline;

import java.util.Arrays;

import org.junit.Test;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class CyclicPathFilterPipeTest {
	@Test
    public void testPipeBasic() {
        Pipe pipe = new Pipeline(new CharacterCountPipe(), new CharacterCountPipe(), new CyclicPathFilterPipe());
        pipe.setStarts(Arrays.asList("1", "11"));
        int counter = 0;
        while (pipe.hasNext()) {
            assertEquals(pipe.next(), "1");
            counter++;
        }
        assertEquals(counter, 1);
    }

    private class CharacterCountPipe extends AbstractPipe<String, String> {
        protected String processNextStart() {
            return "" + this.starts.next().length();
        }
    }

}
