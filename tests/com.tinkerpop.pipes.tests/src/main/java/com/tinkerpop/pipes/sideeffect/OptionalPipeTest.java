package com.tinkerpop.pipes.sideeffect;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.tinkerpop.pipes.filter.FilterPipe;
import com.tinkerpop.pipes.impl.filter.ObjectFilterPipe;
import com.tinkerpop.pipes.impl.sideeffect.AggregatePipe;
import com.tinkerpop.pipes.impl.sideeffect.OptionalPipe;
import com.tinkerpop.pipes.impl.transform.IdentityPipe;
import com.tinkerpop.pipes.impl.util.Pipeline;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class OptionalPipeTest {
	@Test
    public void testPipeBasic() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "josh");
        OptionalPipe<String> pipe1 = new OptionalPipe<String>(new IdentityPipe<String>());
        pipe1.setStarts(names);
        int counter = 0;
        while (pipe1.hasNext()) {
            counter++;
            assertTrue(names.contains(pipe1.next()));
        }
        assertEquals(counter, 4);
    }

	@Test
    public void testOptionalSideEffect() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "josh");
        List<String> results = new ArrayList<String>();
        OptionalPipe<String> pipe1 = new OptionalPipe<String>(new AggregatePipe<String>(results));
        pipe1.setStarts(names);
        int counter = 0;
        while (pipe1.hasNext()) {
            counter++;
            assertTrue(names.contains(pipe1.next()));

        }
        assertEquals(counter, 4);
        for (int i = 0; i < names.size(); i++) {
            assertEquals(names.get(i), results.get(i));
        }
    }

	@Test
    public void testOptionalSideEffectWithFilter() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "josh");
        List<String> results = new ArrayList<String>();
        OptionalPipe<String> pipe1 = new OptionalPipe<String>(new Pipeline(new AggregatePipe<String>(results), new ObjectFilterPipe<String>("marko", FilterPipe.Filter.EQUAL)));
        pipe1.setStarts(names);
        int counter = 0;
        while (pipe1.hasNext()) {
            counter++;
            assertTrue(names.contains(pipe1.next()));

        }
        assertEquals(counter, 4);
        for (int i = 0; i < names.size(); i++) {
            assertEquals(names.get(i), results.get(i));
        }
    }


}
