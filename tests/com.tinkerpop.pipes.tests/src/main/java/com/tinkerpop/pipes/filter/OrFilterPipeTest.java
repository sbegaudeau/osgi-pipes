package com.tinkerpop.pipes.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.tinkerpop.pipes.AbstractPipe;
import com.tinkerpop.pipes.Pipe;
import com.tinkerpop.pipes.impl.filter.ObjectFilterPipe;
import com.tinkerpop.pipes.impl.filter.OrFilterPipe;
import com.tinkerpop.pipes.impl.util.Pipeline;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class OrFilterPipeTest {

	@Test
    public void testOrPipeBasic() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "povel", "marko");
        ObjectFilterPipe<String> pipe1 = new ObjectFilterPipe<String>("marko", FilterPipe.Filter.EQUAL);
        ObjectFilterPipe<String> pipe2 = new ObjectFilterPipe<String>("povel", FilterPipe.Filter.EQUAL);
        OrFilterPipe<String> orFilterPipe = new OrFilterPipe<String>(pipe1, pipe2);
        orFilterPipe.setStarts(names);
        int counter = 0;
        while (orFilterPipe.hasNext()) {
            String name = orFilterPipe.next();
            assertTrue(name.equals("marko") || name.equals("povel"));
            counter++;
        }
        assertEquals(counter, 4);
    }

	@Test
    public void testFutureFilter() {
        List<String> names = Arrays.asList("marko", "peter", "josh", "marko", "jake", "marko", "marko");
        Pipe<String, Integer> pipeA = new CharacterCountPipe();
        Pipe<Integer, Integer> pipeB = new ObjectFilterPipe<Integer>(4, FilterPipe.Filter.NOT_EQUAL);
        Pipe<String, String> pipe1 = new OrFilterPipe<String>(new Pipeline<String, Integer>(pipeA, pipeB));
        Pipeline<String, String> pipeline = new Pipeline<String, String>(pipe1);
        pipeline.setStarts(names);
        int counter = 0;
        while (pipeline.hasNext()) {
            String name = pipeline.next();
            //System.out.println(name);
            counter++;
            assertTrue((name.equals("marko") || name.equals("peter")) && !name.equals("josh") && !name.equals("jake"));
        }
        assertEquals(counter, 5);
    }

    private class CharacterCountPipe extends AbstractPipe<String, Integer> {
        protected Integer processNextStart() {
            return this.starts.next().length();
        }
    }
}
