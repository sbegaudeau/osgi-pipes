package com.tinkerpop.pipes.suite;

import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.textui.TestRunner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.tinkerpop.pipes.AbstractPipeTest;
import com.tinkerpop.pipes.FunctionPipeTest;
import com.tinkerpop.pipes.TimingTest;
import com.tinkerpop.pipes.branch.CopySplitPipeTest;
import com.tinkerpop.pipes.branch.IfThenElsePipeTest;
import com.tinkerpop.pipes.branch.LoopPipeTest;
import com.tinkerpop.pipes.filter.AndFilterPipeTest;
import com.tinkerpop.pipes.filter.BackFilterPipeTest;
import com.tinkerpop.pipes.filter.CyclicPathFilterPipeTest;
import com.tinkerpop.pipes.filter.DuplicateFilterPipeTest;
import com.tinkerpop.pipes.filter.ExceptFilterPipeTest;
import com.tinkerpop.pipes.filter.FilterFunctionPipeTest;
import com.tinkerpop.pipes.filter.FutureFilterPipeTest;
import com.tinkerpop.pipes.filter.ObjectFilterPipeTest;
import com.tinkerpop.pipes.filter.OrFilterPipeTest;
import com.tinkerpop.pipes.filter.RandomFilterPipeTest;
import com.tinkerpop.pipes.filter.RangeFilterPipeTest;
import com.tinkerpop.pipes.filter.RetainFilterPipeTest;
import com.tinkerpop.pipes.sideeffect.AggregatePipeTest;
import com.tinkerpop.pipes.sideeffect.CountPipeTest;
import com.tinkerpop.pipes.sideeffect.GroupByPipeTest;
import com.tinkerpop.pipes.sideeffect.GroupByReducePipeTest;
import com.tinkerpop.pipes.sideeffect.GroupCountFunctionPipeTest;
import com.tinkerpop.pipes.sideeffect.GroupCountPipeTest;
import com.tinkerpop.pipes.sideeffect.OptionalPipeTest;
import com.tinkerpop.pipes.sideeffect.SideEffectFunctionPipeTest;
import com.tinkerpop.pipes.sideeffect.StoragePipeTest;
import com.tinkerpop.pipes.sideeffect.TablePipeTest;
import com.tinkerpop.pipes.sideeffect.TreePipeTest;
import com.tinkerpop.pipes.transform.GatherFunctionPipeTest;
import com.tinkerpop.pipes.transform.GatherPipeTest;
import com.tinkerpop.pipes.transform.HasCountPipeTest;
import com.tinkerpop.pipes.transform.HasNextPipeTest;
import com.tinkerpop.pipes.transform.IdentityPipeTest;
import com.tinkerpop.pipes.transform.MemoizePipeTest;
import com.tinkerpop.pipes.transform.OrderPipeTest;
import com.tinkerpop.pipes.transform.PathPipeTest;
import com.tinkerpop.pipes.transform.ScatterPipeTest;
import com.tinkerpop.pipes.transform.SelectPipeTest;
import com.tinkerpop.pipes.transform.SideEffectCapPipeTest;
import com.tinkerpop.pipes.transform.TransformFunctionPipeTest;
import com.tinkerpop.pipes.util.PipelineTest;
import com.tinkerpop.pipes.util.StartPipeTest;
import com.tinkerpop.pipes.util.iterators.ExpandableIteratorTest;
import com.tinkerpop.pipes.util.iterators.MultiIteratorTest;
import com.tinkerpop.pipes.util.iterators.SingleIteratorTest;
import com.tinkerpop.pipes.util.structures.ArrayQueueTest;
import com.tinkerpop.pipes.util.structures.TableTest;
import com.tinkerpop.pipes.util.structures.TreeTest;

@RunWith(Suite.class)
@SuiteClasses({
	AbstractPipeTest.class, FunctionPipeTest.class, TimingTest.class,
	CopySplitPipeTest.class, IfThenElsePipeTest.class, LoopPipeTest.class,
	AndFilterPipeTest.class, BackFilterPipeTest.class, CyclicPathFilterPipeTest.class,
	DuplicateFilterPipeTest.class, ExceptFilterPipeTest.class, FilterFunctionPipeTest.class,
	FutureFilterPipeTest.class, ObjectFilterPipeTest.class, OrFilterPipeTest.class,
	RandomFilterPipeTest.class, RangeFilterPipeTest.class, RetainFilterPipeTest.class,
	AggregatePipeTest.class, CountPipeTest.class, GroupByPipeTest.class,
	GroupByReducePipeTest.class, GroupCountFunctionPipeTest.class, GroupCountPipeTest.class,
	OptionalPipeTest.class, SideEffectFunctionPipeTest.class, StoragePipeTest.class,
	TablePipeTest.class, TreePipeTest.class, GatherFunctionPipeTest.class, GatherPipeTest.class,
	HasCountPipeTest.class, HasNextPipeTest.class, IdentityPipeTest.class,
	MemoizePipeTest.class, OrderPipeTest.class, PathPipeTest.class,
	ScatterPipeTest.class, SelectPipeTest.class, SideEffectCapPipeTest.class,
	TransformFunctionPipeTest.class, PipelineTest.class, StartPipeTest.class,
	ExpandableIteratorTest.class, MultiIteratorTest.class, SingleIteratorTest.class,
	ArrayQueueTest.class, TableTest.class, TreeTest.class,})
public class PipesSuite {
	/**
	 * Launches the test with the given arguments.
	 * 
	 * @param args
	 *            Arguments of the testCase.
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * Creates the {@link junit.framework.TestSuite TestSuite} for all the test.
	 * 
	 * @return The test suite containing all the tests
	 */
	public static Test suite() {
		return new JUnit4TestAdapter(PipesSuite.class);
	}
}
