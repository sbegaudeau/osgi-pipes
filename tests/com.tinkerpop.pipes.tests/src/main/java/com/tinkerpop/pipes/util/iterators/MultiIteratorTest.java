package com.tinkerpop.pipes.util.iterators;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Test;

import com.tinkerpop.pipes.util.impl.iterators.MultiIterator;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class MultiIteratorTest {
	@Test
    public void testMultiIteratorSimple() {
        List<Integer> a = Arrays.asList(1, 2);
        List<Integer> b = Arrays.asList(3, 4, 5);
        List<Integer> c = Arrays.asList(6, 7, 8);
        Iterator<Integer> itty = new MultiIterator<Integer>(a.iterator(), b.iterator(), c.iterator());
        int counter = 0;
        while (itty.hasNext()) {
            counter++;
            assertEquals(itty.next(), (Integer) counter);
        }
        assertEquals(counter, 8);

    }

	@Test
    public void testMultiIteratorPureNext() {
        List<Integer> a = Arrays.asList(1, 2);
        List<Integer> b = Arrays.asList(3, 4, 5);
        List<Integer> c = Arrays.asList(6, 7, 8);
        Iterator<Integer> itty = new MultiIterator<Integer>(a.iterator(), b.iterator(), c.iterator());
        int counter = 0;
        try {
            while (true) {
                counter++;
                assertEquals(itty.next(), (Integer) counter);
            }
        } catch (NoSuchElementException e) {
            assertEquals(counter, 9);
        }

    }

	@Test
    public void testMultiIteratorNoParameters() {
        Iterator<Integer> itty = new MultiIterator<Integer>();
        int counter = 0;
        while (itty.hasNext()) {
            counter++;
        }
        assertEquals(counter, 0);
    }
}
