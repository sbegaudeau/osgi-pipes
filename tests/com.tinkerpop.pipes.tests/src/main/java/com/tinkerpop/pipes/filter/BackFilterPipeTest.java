package com.tinkerpop.pipes.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.tinkerpop.pipes.impl.filter.BackFilterPipe;
import com.tinkerpop.pipes.impl.filter.ExceptFilterPipe;
import com.tinkerpop.pipes.filter.FilterPipe;
import com.tinkerpop.pipes.impl.transform.IdentityPipe;
import com.tinkerpop.pipes.util.PipeHelper;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class BackFilterPipeTest {

	@Test
    public void testBackFilterWithNoFilter() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "josh");
        FilterPipe<String> pipe1 = new BackFilterPipe<String>(new IdentityPipe<String>());
        pipe1.setStarts(names);
        assertEquals(PipeHelper.counter(pipe1), 4);
    }

	@Test
    public void testBackFilterWithFilter() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "josh");
        BackFilterPipe<String> pipe1 = new BackFilterPipe<String>(new ExceptFilterPipe<String>(Arrays.asList("marko", "povel")));
        pipe1.setStarts(names);
        int counter = 0;
        while (pipe1.hasNext()) {
            counter++;
            String name = pipe1.next();
            assertTrue(name.equals("peter") || name.equals("josh"));
        }
        assertEquals(counter, 2);
    }

}
