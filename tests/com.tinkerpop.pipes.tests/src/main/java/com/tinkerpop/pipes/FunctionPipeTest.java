package com.tinkerpop.pipes;

import static org.junit.Assert.assertEquals;

import com.tinkerpop.pipes.util.PipeHelper;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class FunctionPipeTest {

	@Test
    public void testFunctionPipeWithIdentity() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "josh");
        FunctionPipe<String, String> pipe = new FunctionPipe<String, String>(new IdentityPipeFunction());
        pipe.setStarts(names);
        assertEquals(PipeHelper.counter(pipe), 4);
        pipe.reset();
        pipe.setStarts(names);
        PipeHelper.areEqual(names.iterator(), pipe.iterator());
    }

	@Test
    public void testFunctionPipeWithFilter() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "josh");
        FunctionPipe<String, String> pipe = new FunctionPipe<String, String>(new StartsWithPipeFunction());
        pipe.setStarts(names);
        assertEquals(PipeHelper.counter(pipe), 2);
        pipe.reset();
        pipe.setStarts(names);
        PipeHelper.areEqual(Arrays.asList("povel", "peter").iterator(), pipe.iterator());
    }

    private class IdentityPipeFunction implements PipeFunction<Iterator<String>, String> {
        public String compute(Iterator<String> argument) {
            return argument.next();
        }
    }

    private class StartsWithPipeFunction implements PipeFunction<Iterator<String>, String> {
        public String compute(Iterator<String> argument) {
            while (true) {
                String s = argument.next();
                if (s.startsWith("p"))
                    return s;
            }
        }
    }
}
