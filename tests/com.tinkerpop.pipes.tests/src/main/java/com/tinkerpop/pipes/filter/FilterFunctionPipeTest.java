package com.tinkerpop.pipes.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.tinkerpop.pipes.PipeFunction;
import com.tinkerpop.pipes.impl.filter.FilterFunctionPipe;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class FilterFunctionPipeTest {

	@Test
    public void testPipeBasic() {
        List<String> names = Arrays.asList("marko", "povel", "peter", "povel", "marko");
        FilterPipe<String> pipe = new FilterFunctionPipe<String>(new StartsWithPipeFunction());
        pipe.setStarts(names);
        int counter = 0;
        while (pipe.hasNext()) {
            String name = pipe.next();
            assertTrue(name.equals("povel") || name.equals("peter"));
            counter++;
        }
        assertEquals(counter, 3);

    }

    private class StartsWithPipeFunction implements PipeFunction<String, Boolean> {
        public Boolean compute(String argument) {
            return argument.startsWith("p");
        }
    }
}
