package com.tinkerpop.pipes.transform;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Test;

import com.tinkerpop.pipes.Pipe;
import com.tinkerpop.pipes.impl.sideeffect.CountPipe;
import com.tinkerpop.pipes.impl.transform.SideEffectCapPipe;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class SideEffectCapPipeTest {
	@Test
    public void testSideEffectCapPipeNormalCount() {
        List<String> list = Arrays.asList("marko", "antonio", "rodriguez", "was", "here", ".");
        Pipe<String, Long> pipe = new SideEffectCapPipe<String, Long>(new CountPipe<String>());
        pipe.setStarts(list.iterator());
        assertTrue(pipe.hasNext());
        assertTrue(pipe.hasNext());
        assertEquals(pipe.next(), new Long(6));
        assertFalse(pipe.hasNext());
        try {
            pipe.next();
            assertTrue(false);
        } catch (NoSuchElementException e) {
            assertFalse(false);
        }
        pipe.reset();
        pipe.setStarts(list);
        assertTrue(pipe.hasNext());
        assertEquals(6, (long) pipe.next());
    }

	@Test
    public void testSideEffectCapPipeZeroCount() {
        List<String> list = Arrays.asList();
        Pipe<String, Long> pipe = new SideEffectCapPipe<String, Long>(new CountPipe<String>());
        pipe.setStarts(list.iterator());
        assertTrue(pipe.hasNext());
        assertTrue(pipe.hasNext());
        assertEquals(pipe.next(), new Long(0));
        assertFalse(pipe.hasNext());
        try {
            pipe.next();
            assertTrue(false);
        } catch (NoSuchElementException e) {
            assertFalse(false);
        }
    }
}
