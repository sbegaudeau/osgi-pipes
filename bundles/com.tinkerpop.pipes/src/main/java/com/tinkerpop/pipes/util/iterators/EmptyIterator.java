package com.tinkerpop.pipes.util.iterators;

import java.util.Iterator;

import com.tinkerpop.pipes.util.FastNoSuchElementException;

/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class EmptyIterator<T> implements Iterator<T> {

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public boolean hasNext() {
        return false;
    }

    public T next() {
        throw FastNoSuchElementException.instance();
    }
}